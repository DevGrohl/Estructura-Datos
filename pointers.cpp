#include <iostream>

using namespace std;

struct Node {
	int var;
	float* q; 
	Node* pointer;
}registro, nodo;

int main(){
	int var = 10;
	int* p;
	int* q;

	p = &var;
	q = p;

	cout << q << " == " << p << endl;
	cout << *q << endl;
        
    float x = 3.1416;

    registro.var = 18;
    registro.q = &x;
	cout << registro.var << endl;
	cout << registro.q << endl;
	cout << *registro.q << endl;

	nodo.var = 19;
	registro.pointer = &nodo;

	cout << "--------------- POINTER TO NODE -------------" << endl;

	cout << registro.pointer << endl;
	cout << registro.pointer->var << endl;

    return 0;
}
