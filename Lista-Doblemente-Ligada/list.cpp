#include "list.h"


// Node<T>* tail;
// Node<T>* head;
// int size;

template <typename T>
List<T>::List()
{
        this->init();
        std::cout << "Inicializando lista" << std::endl;
}

template <typename T>
void List<T>::menu()
{
        int value = 0;
        int position = 0;
        char option = 0;

        do{
                std::cout << std::endl
                << "1) Insertar un nodo al inicio" << std::endl
                << "2) Insertar un nodo al final" << std::endl
                << "4) Eliminar un nodo al inicio" << std::endl
                << "5) Eliminar un nodo al final" << std::endl
                << "9) Limpiar la lista" << std::endl
                << "0) Mostrar lista" << std::endl
                << "q) Salir" << std::endl
                << "Selecciona una opcion: ";

                std::cin >> option;
                switch(option){
                        case '1':
                                this->pushB();
                                break;
                        case '2':
                                this->pushE();
                                break;
                        case '4':
                                this->popB();
                                break;
                        case '5':
                                this->popE();
                                break;
                        case '9':
                                this->empty();
                                break;
                        case '0':
                                std::cout << std::endl << "------- READ LIST -------" << std::endl;
                                Node<videogame>* tail;
                                Node<videogame>* head;
                                tail = this->getTail();
                                head = this->getHead();
                                std::cout << "Tail: " << tail << std::endl;
                                std::cout << "Head: " << head << std::endl;

                                while(tail != NULL){
                                        std::cout << "----------------------------" << std::endl;
                                        std::cout << "Address: " << tail << std::endl;
                                        std::cout << "Value: " << tail->getContent().getName() << std::endl;
                                        std::cout << "Prev: " << tail->getPrev() << std::endl;
                                        std::cout << "Next: " << tail->getNext() << std::endl;
                                        tail = tail->getNext();
                                }
                                break;
                        case 'q':
                        break;
                        default:
                        std::cout << "La opcion ingresada no es valida, intente de nuevo" << std::endl;
                        break;
                }
        }
        while(option != 'q');
}

template <typename T>
void List<T>::init()
{
        this->head = NULL;
        this->tail = NULL;
        this->size = 0;
}

template <typename T>
void List<T>::pushB()
{
        std::cout << "Insertando nodo al inicio" << std::endl;
        Node<videogame>* aux = new Node<videogame>();
        videogame vg;
        aux->setContent(vg);
        aux->editContent();

        if(isEmpty()){
                this->setHead(aux);
        } else {
                aux->setNext(this->getTail());
                this->getTail()->setPrev(aux);
        }

        this->setTail(aux);
        this->size++;
}

template <typename T>
void List<T>::pushE()
{
        std::cout << "Insertando nodo al final" << std::endl;
        Node<videogame>* aux = new Node<videogame>();
        videogame vg;
        aux->setContent(vg);
        aux->editContent();

        if(isEmpty()){
                this->setTail(aux);
        } else {
                aux->setPrev(this->getHead());
                this->getHead()->setNext(aux);
        }

        this->setHead(aux);
        this->size++;
}

template <typename T>
void List<T>::popB()
{
        if(!this->isEmpty()){
                Node<videogame>* aux = this->getTail();

                if (this->getTail() == this->getHead()){
                        this->setHead(aux->getPrev());
                }

                this->setTail(aux->getNext());
                
                if(NULL != aux->getNext()){
                        aux->getNext()->setPrev(NULL);
                }

                delete(aux);
                this->size--;
        }
}

template <typename T>
void List<T>::popE()
{
        if(!this->isEmpty()){
                Node<videogame>* aux = this->getHead();

                if (this->getTail() == this->getHead()){
                        this->setTail(aux->getNext());
                }
                
                this->setHead(aux->getPrev());
                
                if(NULL != aux->getPrev()){
                        aux->getPrev()->setNext(NULL);
                }

                delete(aux);
                this->size--;
        }
}

template <typename T>
void List<T>::empty()
{
        Node<videogame>* aux;
        aux = this->getTail();

        while(aux != NULL) {
                this->setTail(aux->getNext());
                delete(aux);
                aux = this->getTail();
        }
        this->setTail(NULL);
        this->setHead(NULL);
}

template <typename T>
bool List<T>::isEmpty() { return (this->head) ? false : true; }

template <typename T>
Node<T>* List<T>::getTail() {return this->tail;}
template <typename T>
void List<T>::setTail(Node<T>* tail) {this->tail = tail; }
template <typename T>
Node<T>* List<T>::getHead() {return this->head;}
template <typename T>
void List<T>::setHead(Node<T>* head) {this->head = head;}
