#include "videogame.h"
#include <iostream>

videogame::videogame(){
	std::cout << "Creando videojuego" << std::endl;
}

videogame::videogame(string name, string classification, string genre, string platform, float price){
	std::cout << "Creando videojuego: " << name << std::endl;
	this->name = name;
	this->classification = classification;
	this->genre = genre;
	this->platform = platform;
	this->price = price;
}

// ~videogame::videogame(){
// 	std::cout << "Eliminando videojuego" << std::endl;
// }

void videogame::showMenu(){
    char opc = 0;
    float price;
    std::string value;
    do{
        std::cout << "1) Editar nombre" << std::endl
                << "2) Editar clasificacion" << std::endl
                << "3) Editar genero" << std::endl
                << "4) Editar plataforma" << std::endl
                << "5) Editar precio" << std::endl
                << "0) Salir" << std::endl
                << "Selecciona una opcion : ";

        std::cin >> opc;
        std::cin.ignore();
        
        switch(opc){
            case '1':
                    std::cout << "Ingrese el nombre del videojuego: " ;
                    std::getline(std::cin, value);
                    this->setName(value);
                    break;
            case '2':
                    std::cout << "Ingrese la clasificacion del videojuego: ";
                    std::cin >> value;
                    this->setClassification(value);
                    break;
            case '3':
                    std::cout << "Ingrese el genero del videojuego: ";
                    std::cin >> value;
                    this->setGenre(value);
                    break;
            case '4':
                    std::cout << "Ingrese las plataformas del videojuego: ";
                    std::cin >> value;
                    this->setPlatform(value);
                    break;
            case '5':
                    std::cout << "Ingrese el precio del videojuego: $";
                    std::cin >> price;
                    this->setPrice(price);
                    break;
            case '0':
                    break;
            default:
                    std::cout << "Opcion invalida, intente de nuevo" << std::endl;
                    std::getline(std::cin, value);
                    break;
        }
    }while(opc != '0');
}

string videogame::getName(){
	return this->name;
}
void videogame::setName(string name){
	this->name = name;
}

string videogame::getClassification(){
	return this->classification;
}
void videogame::setClassification(string classification){
	this->classification = classification;
}

string videogame::getGenre(){
	return this->genre;
}
void videogame::setGenre(string genre){
	this->genre = genre;
}

string videogame::getPlatform(){
	return this->platform;
}
void videogame::setPlatform(string platform){
	this->platform = platform;
}

float videogame::getPrice(){
	return this->price;
}
void videogame::setPrice(float price){
	this->price = price;
}
