
#include "node.cpp"

template <typename T> class List{
        private:
                Node<T>* tail;
                Node<T>* head;
                int size;

        public:
                List();

                void init();
                void menu();

                void pushB();
                void pushE();
                
                void popB();
                void popE();

                void empty();
                bool isEmpty();
    
                Node<T>* getTail();
                void setTail(Node<T>*);

                Node<T>* getHead();
                void setHead(Node<T>*);
};
