#include "node.h"

template <typename T>
Node<T>::Node()
{
    std::cout << "Inicializando nodo" << std::endl;
}

template <typename T>
T Node<T>::getContent(){
    return this->content;
}

template <typename T>
void Node<T>::setContent(T content){
    this->content = content;
}

// AD-HOC
template <typename T>
void Node<T>::editContent(){
	this->content.showMenu();
}

template <typename T>
Node<T>* Node<T>::getNext(){
    return this->next;
}

template <typename T>
void Node<T>::setNext(Node<T>* next){
    this->next = next;
}

template <typename T>
Node<T>* Node<T>::getPrev(){
    return this->prev;
}

template <typename T>
void Node<T>::setPrev(Node<T>* prev){
    this->prev = prev;
}
