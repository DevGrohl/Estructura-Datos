#include "list.h"

template <typename T>
List<T>::List()
{
        this->init();
        std::cout << "Inicializando lista" << std::endl << std::endl;
}

template <typename T>
void List<T>::menu()
{
        int value = 0;
        int position = 0;
        char option = 0;
        videogame content;

        do{
                std::cout << std::endl
                << "1) Insertar un nodo al inicio" << std::endl
                << "2) Eliminar un nodo al inicio" << std::endl
                << "3) Mover a la izquierda" << std::endl
                << "4) Mover a la derecha" << std::endl
                << "5) Mostrar contenido" << std::endl
                << "6) Editar contenido" << std::endl
                << "9) Limpiar la lista" << std::endl
                << "0) Mostrar lista" << std::endl
                << "q) Salir" << std::endl
                << "Selecciona una opcion: ";

                std::cin >> option;
                switch(option){
                        case '1':
                                this->push();
                                break;
                        case '2':
                                this->pop();
                                break;
                        case '3':
                                this->toLeft();
                                break;
                        case '4':
                                this->toRight();
                                break;
                        case '5':
                                if(!this->isEmpty()){
                                        content = this->getHead()->getContent();

                                        std::cout << "-*-*-*-*-*- CONTENT -*-*-*-*-*-*-" << std::endl;
                                        std::cout << "Memory Address: " << this->getHead() << std::endl;
                                        std::cout << "Name: " << content.getName() << std::endl;
                                        std::cout << "Price: $" << content.getPrice() << std::endl;
                                        std::cout << "Genre: " << content.getGenre() << std::endl;
                                        std::cout << "Classification: " << content.getClassification() << std::endl;
                                        std::cout << "Platforms: " << content.getPlatform() << std::endl;
                                } else {
                                        std::cout << "~*~*~*~ EMPTY LIST ~*~*~*~" << std::endl;
                                }
                                break;
                        case '6':
                                this->getHead()->editContent();
                                break;
                        case '9':
                                this->empty();
                                break;
                        case '0':
                                std::cout << std::endl << "------- READ LIST -------" << std::endl;
                                if (!this->isEmpty()) {
                                        Node<videogame>* head;
                                        head = this->getHead();
                                        std::cout << "Head: " << head << std::endl;
                                        
                                        do{
                                                std::cout << "----------------------------" << std::endl;
                                                std::cout << "Address: " << head << std::endl;
                                                std::cout << "Value: " << head->getContent().getName() << std::endl;
                                                std::cout << "Prev: " << head->getPrev() << std::endl;
                                                std::cout << "Next: " << head->getNext() << std::endl;
                                                head = head->getNext();
                                        }while(head != this->getHead());
                                } else {
                                        std::cout << "~*~*~*~ EMPTY LIST ~*~*~*~" << std::endl;
                                }
                                break;
                        case 'q':
                                break;
                        default:
                                std::cout << "La opcion ingresada no es valida, intente de nuevo" << std::endl;
                                break;
                }
        }
        while(option != 'q');
}

template <typename T>
void List<T>::init()
{
        this->head = NULL;
        this->size = 0;
}

template <typename T>
void List<T>::push()
{
        std::cout << "Insertando nodo al inicio" << std::endl;
        Node<videogame>* aux = new Node<videogame>();
        videogame vg;
        aux->setContent(vg);
        aux->editContent();

        if(isEmpty()){
                aux->setNext(aux);
                aux->setPrev(aux);
        } else {
                this->getHead()->getPrev()->setNext(aux);
                aux->setPrev(this->getHead()->getPrev());

                this->getHead()->setPrev(aux);
                aux->setNext(this->getHead());
        }

        this->setHead(aux);
        this->size++;
}

template <typename T>
void List<T>::pop()
{
        if(!this->isEmpty()){
                Node<videogame>* aux = this->getHead();

                if ((aux->getNext() == this->getHead())){
                        this->setHead(NULL);
                } else {
                        aux->getNext()->setPrev(aux->getPrev());
                        aux->getPrev()->setNext(aux->getNext());
                        this->setHead(aux->getNext());
                }

                delete(aux);

                this->size--;
        }
}

template <typename T>
void List<T>::empty()
{
        while(!this->isEmpty()){
                this->pop();
        }
}

template <typename T>
bool List<T>::isEmpty() { return (this->head) ? false : true; }

template <typename T>
Node<T>* List<T>::getHead() {return this->head;}

template <typename T>
void List<T>::setHead(Node<T>* head) {this->head = head;}

template <typename T>
void List<T>::toLeft() { this->head = this->head->getPrev(); }

template <typename T>
void List<T>::toRight() { this->head = this->head->getNext(); }