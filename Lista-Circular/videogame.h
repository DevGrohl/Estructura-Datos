#include <string>
using namespace std;

class videogame
{
private: 
	string name;
	string classification;
	string genre;
	string platform;
	float price;

public:
	videogame();
	videogame(string, string, string, string, float);
	// ~videogame();

    void showMenu();

	string getName();	
	void setName(string); 

	string getClassification();	
	void setClassification(string); 

	string getGenre();	
	void setGenre(string); 

	string getPlatform();	
	void setPlatform(string); 

	float getPrice();	
	void setPrice(float); 
};
