#include "videogame.cpp"
#include <iostream>

template <typename T> class Node {
    private:
        T content;
        Node<T>* next;
        Node<T>* prev;

    public:
        Node();

        T getContent();
        void setContent(T);
        void editContent();

        Node* getNext();
        void setNext(Node<T>*);

        Node* getPrev();
        void setPrev(Node<T>*);
};