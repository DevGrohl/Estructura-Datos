
#include "node.cpp"

template <typename T> class List{
        private:
                Node<T>* head;
                int size;

        public:
                List();

                void init();
                void menu();

                void push();
                void pop();

                void toLeft();
                void toRight();

                void empty();
                bool isEmpty();
    
                Node<T>* getHead();
                void setHead(Node<T>*);
};
