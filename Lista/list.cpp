#include "list.h"

List::List()
{
        this->init();
        std::cout << "Inicializando lista" << std::endl;
}

void List::menu()
{
        int value = 0;
        int position = 0;
        char option = 0;

        do{
                std::cout << std::endl
                << "1) Insertar un nodo al inicio" << std::endl
                << "2) Insertar un nodo al final" << std::endl
                << "3) Insertar un nodo en una posicion" << std::endl
                << "4) Eliminar un nodo al inicio" << std::endl
                << "5) Eliminar un nodo al final" << std::endl
                << "6) Eliminar un nodo en una posicion" << std::endl
                << "7) Buscar un nodo en una posicion" << std::endl
                << "8) Buscar un nodo por un valor" << std::endl
                << "9) Limpiar la lista" << std::endl
                << "0) Mostrar lista" << std::endl
                << "q) Salir" << std::endl
                << "Selecciona una opcion: ";

                std::cin >> option;
                switch(option){
                        case '1':
                                std::cout << "Ingresa el valor del nodo: ";
                                std::cin >> value;
                                this->pushB(value);
                                break;
                        case '2':
                                std::cout << "Ingresa el valor del nodo: ";
                                std::cin >> value;
                                this->pushE(value);
                                break;
                        case '3':
                                std::cout << "Ingresa el valor del nodo: ";
                                std::cin >> value;
                                std::cout << "Ingresa la posicion del nodo: ";
                                std::cin >> position;
                                this->pushP(value, position);
                                break;
                        case '4':
                                this->popB();
                                break;
                        case '5': 
                                this->popE();
                                break;
                        case '6':
                                std::cout << "Ingresa la posicion del nodo: ";
                                std::cin >> position;
                                this->popP(position);
                                break;
                        case '7':
                                std::cout << "Ingresa la posicion del nodo: ";
                                std::cin >> position;
                                this->searchP(position);
                                break;
                        case '8':
                                std::cout << "Ingresa el valor del nodo: ";
                                std::cin >> value;
                                this->searchV(value);
                                break;
                        case '9':
                                this->empty();
                                break;
                        case '0':
                                std::cout << "------- READ LIST -------" << std::endl;
                                Node* last;
                                last = this->getAncla();
                                std::cout << "Ancla: " << last << std::endl;

                                while(last != NULL){
                                        std::cout << "----------------------------" << std::endl;
                                        std::cout << "Address: " << last << std::endl;
                                        std::cout << "Value: " << last->getValue() << std::endl;
                                        std::cout << "Pointer: " << last->getPointer() << std::endl;
                                        last = last->getPointer();
                                }
                                break;
                        case 'q':
                                break;
                        default: 
                                std::cout << "La opcion ingresada no es valida, intente de nuevo" << std::endl;
                                break;
                }
        }
        while(option != 'q');
}

void List::init()
{
        this->ancla = NULL;
        this->size = 0;
}

void List::pushB(int value)
{
        std::cout << "Insertando nodo al inicio" << std::endl;
        Node* aux = new Node(value, this->getAncla());

        this->setAncla(aux);
        this->size++;
}

void List::pushE(int value)
{
        std::cout << "Insertando nodo al final" << std::endl;
        Node* aux = new Node(value);

        if (this->isEmpty()){
                this->setAncla(aux);
        } else {
                Node* temp = this->getAncla();
                while(temp->getPointer() != NULL){
                        temp = temp->getPointer();
                }
                temp->setPointer(aux);
        }        
        this->size++;
}

void List::pushP(int value, int pos)
{
        std::cout << "Insertando en la posicion: " << pos << std::endl;
        Node* aux = new Node(value);

        if (this->isEmpty()){
                this->pushB(value);
        } else {
                Node* temp = this->getAncla();
                Node* past;
                for (int i = 1; i < pos; ++i){
                        past = temp;
                        if(temp->getPointer() == NULL){
                                pushE(value);
                                return;
                        } else {
                                temp = temp->getPointer();
                        }
                }
                past->setPointer(aux);
                aux->setPointer(temp);
                this->size++;
        }
}

void List::popB()
{
        Node* temp = this->getAncla();

        if(!this->isEmpty()){
                this->setAncla(temp->getPointer());
                delete(temp);
                this->size--;
        }
}

void List::popE()
{
        Node* temp = this->getAncla();
        Node* past;

        if(!this->isEmpty()){
                while(temp->getPointer() != NULL){
                        past = temp;
                        temp = temp->getPointer();
                }

                past->setPointer(NULL);
                delete(temp);
                this->size--;
        }
}

void List::popP(int pos)
{
        if (this->isEmpty()){
                std::cout << "La lista ya está vacia" << std::endl;
        } else {
                Node* temp = this->getAncla();
                Node* past = temp;
                for (int i = 1; i < pos; ++i){
                        past = temp;
                        if(temp->getPointer() == NULL){
                                std::cout << "El elemento que buscas no existe" << std::endl;
                                return;
                        }
                        temp = temp->getPointer();
                }
                past->setPointer(temp->getPointer());
                delete(temp);
                this->size--;
        }
}


void List::empty()
{
        Node* temp = this->getAncla();
        while(this->getAncla() != NULL){
                this->setAncla(temp->getPointer());
                delete(temp);
                temp = this->getAncla();
                this->size--;
        }
}

bool List::isEmpty() { return (ancla) ? false : true; }

int List::searchP(int pos)
{
        Node* temp = this->getAncla();
        std::cout << "SIZE: " << this->size << std::endl;
        if (this->size >= pos){
                std::cout << " ----- " << std::endl;
                for (int i = 1; i < pos; ++i)
                {
                        temp = temp->getPointer();
                }
                return temp->getValue();
        }
}

int List::searchV(int value)
{
        Node* temp = this->getAncla();
        int pos = 1;
        if(!this->isEmpty()) {
                while(temp->getPointer() != NULL) {
                        if (temp->getValue() == value){
                                return pos;
                        }
                        temp = temp->getPointer();
                        pos++;
                }                
        }
        return 0;
}

Node* List::getAncla() { return this->ancla; }

void List::setAncla(Node* p) { this->ancla = p; }
