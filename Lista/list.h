#include "node.cpp"
using namespace std;

class List{
        private:
                Node* ancla;
                int size;

        public:
                List();

                void menu();

                void init();
                void pushB(int);
                void pushE(int);
                void pushP(int, int);
                
                void popB();
                void popE();
                void popP(int);

                void empty();
                bool isEmpty();

                int searchP(int);
                int searchV(int);
    
                Node* getAncla();
                void setAncla(Node*);
};
