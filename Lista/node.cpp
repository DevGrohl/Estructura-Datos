#include "node.h"

//int value;
//Node* pointer;

Node::~Node()
{
    std::cout << "Liberando espacio de memoria: " << this->value << std::endl;
}

Node::Node()
{
        std::cout << "Inicializando nodo" << std::endl;
}

Node::Node(int value)
{
    this->value = value;
    this->pointer = NULL;

    std::cout << "Inicializando nodo" << std::endl;
    std::cout << "Valor del nodo: " << this->value << std::endl;
    std::cout << "Direccion del apuntador: " << this->pointer << std::endl;
}

Node::Node(int value, Node* pointer)
{
    this->value = value;
    this->pointer = pointer;

    std::cout << "Inicializando nodo" << std::endl;
    std::cout << "Valor: " << this->value << std::endl;
    std::cout << "Direccion: " << this->pointer << std::endl;
}

int Node::getValue(){
    return this->value;
}

void Node::setValue(int value){
    this->value = value;
}

Node* Node::getPointer(){
    return this->pointer;
}

void Node::setPointer(Node* pointer){
    this->pointer = pointer;
}

